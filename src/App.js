import React, { Suspense } from 'react';
import logo from './logo.svg';
import { Route, Link, BrowserRouter as Router, Switch } from 'react-router-dom'
import './App.css';
import PrivateRoute from './js/components/PrivateRoute'

const Login = React.lazy(() => import('./js/containers/Login'));
const Register = React.lazy(() => import('./js/containers/Register'));
const Dashboard = React.lazy(() => import('./js/containers/Dashboard'));
const Cart = React.lazy(() => import('./js/containers/Cart'))
const Orders = React.lazy(() => import('./js/containers/Orders'))



function App() {

  return (
    <div>
      <Suspense fallback={<div>Loading...</div>}>
        <Router>
          <Switch>
            <Route exact path="/" component={Login} />
            <Route path="/register" component={Register} />
            <Route path="/dashboard" component={Dashboard} />
            <PrivateRoute path="/orders" component={Orders} />
            <PrivateRoute path="/cart" component={Cart} />
          </Switch>
        </Router>
      </Suspense>
    </div>
  );
}

export default App;
