import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Layout from '../Layout/Layout'

export default function AlertDialog(props) {

    const { isOpen, handleClose, modalTitle, modalDescription, modalFooter } = props

    // console.log(props)

    return (
        <div>
            <Dialog
                open={isOpen}
                onClose={() => { handleClose(false) }}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{modalTitle}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {modalDescription}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    {modalFooter}
                </DialogActions>
            </Dialog>
        </div>
    );
}
