import React from 'react';
import renderer from 'react-test-renderer';
import { mount, shallow } from 'enzyme';

import Login from '../Login';

describe('Login Form', () => {
    it('Input Fields like username and password', () => {
        const wrapper = mount(<Login />);
        expect(wrapper.find('input')).toHaveLength(2);
    });
    it('Check submit button', () => {
        const wrapper = mount(<Login />);
        expect(wrapper.find('button')).toHaveLength(1);
    });
});

describe('Login Form Snapshot', () => {
    it('Input Fields like username and password', () => {
        const wrapper = mount(<Login />);
        expect(wrapper).toMatchSnapshot()
    });
});
