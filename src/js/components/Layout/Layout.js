import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import Paper from '@material-ui/core/Paper';
import Card from '../Card/Card'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    height: 140,
    width: 100,
  },
  control: {
    padding: theme.spacing(2),
  },
}));

/**
 * 
 * @param {*} props
 * This component to specify the layout of the card 
 */
export default function SpacingGrid(props) {

  const classes = useStyles();
  
  const { items,direction} = props;

  return (
    <Grid container className={classes.root} spacing={2}>
      <Grid item xs={12}>
        <Grid container justify="flex-start" direction = {direction} spacing={8}>
          {items.map((item) => (
            <Grid key={item.id} item>
              <Card item={item} {...props} />
            </Grid>
          ))}
        </Grid>
      </Grid>
    </Grid>
  );
}
