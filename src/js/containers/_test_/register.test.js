import React from 'react';
import renderer from 'react-test-renderer';
import { mount, shallow } from 'enzyme';

import Register from '../Register';

describe('Register Form', () => {
    it('Input Fields like username and password', () => {
        const wrapper = mount(<Register />);
        expect(wrapper.find('input')).toHaveLength(5);
    });
    it('Check register button', () => {
        const wrapper = mount(<Register />);
        expect(wrapper.find('button')).toHaveLength(1);
    });
});

describe('Register Form Snapshot', () => {
    it('Input Fields like username and password', () => {
        const wrapper = mount(<Register />);
        expect(wrapper).toMatchSnapshot()
    });
});