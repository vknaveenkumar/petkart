import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import axios from 'axios';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import { withStyles } from "@material-ui/core/styles";
// import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {BASE_URL} from '../../config/env'

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                Your Website
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}



const styles = theme => ({
    root: {
        backgroundColor: "red"
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },

});

export class Register extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            fname: '',
            lname: '',
            cpassword: '',
            errorText : '',
            onSubmit: false,
            loginError: false
        }
    }

    _handleTextFieldChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    _handleRegister = () => {
        const { username, password, fname, lname,cpassword } = this.state

        this.setState({
            onSubmit: true
        })


        if (username !== '' && password !== '' && fname !== '' && lname !== '') {
            if (password === cpassword) {
                axios.post(`${BASE_URL}/users`, {
                    username: username,
                    password: password,
                    fname: fname,
                    lname: lname
                }).then(res => {
                    console.log("logn response", res)
                    axios.post(`${BASE_URL}/carts`, {
                        userId: res.data.id,
                        cart: [],
                    }).then(res => {
                        console.log(res)
                        this.props.history.push('/')
                    })
                })
            } else {
                this.setState({
                    errorText : 'Password and Confirm Password are not same'
                })
            }

        }


    }

    render() {
        const { classes } = this.props;
        const { username, password, fname, lname,cpassword,errorText, onSubmit } = this.state
        return (
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>

                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Register your account
                    </Typography>
                    <form className={classes.form} noValidate>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="User Name"
                            name="username"
                            autoComplete="username"
                            value={this.state.textFieldValue}
                            onChange={this._handleTextFieldChange}
                            autoFocus
                            error={onSubmit && username.length <= 0}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="First Name"
                            name="fname"
                            autoComplete="username"
                            value={this.state.textFieldValue}
                            onChange={this._handleTextFieldChange}
                            autoFocus
                            error={onSubmit && fname.length <= 0}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Last Name"
                            name="lname"
                            autoComplete="username"
                            value={this.state.textFieldValue}
                            onChange={this._handleTextFieldChange}
                            autoFocus
                            error={onSubmit && lname.length <= 0}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            value={this.state.textFieldValue}
                            onChange={this._handleTextFieldChange}
                            autoComplete="current-password"
                            error={onSubmit && password.length <= 0}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="cpassword"
                            label="Confirm Password"
                            type="password"
                            id="password"
                            value={this.state.textFieldValue}
                            onChange={this._handleTextFieldChange}
                            autoComplete="current-password"
                            error={onSubmit && cpassword.length <= 0}
                        />
                         {errorText.length>0 &&
                            <Typography variant="body2" color="error" component="p">
                              {errorText}
                        </Typography>}

                        <Button

                            fullWidth
                            variant="contained"
                            color="primary"
                            onClick={this._handleRegister}
                            className={classes.submit}
                        >
                            Register
                        </Button>
                        <Grid container>
                            <Grid item>
                                <Link href="/" variant="body2">
                                    {"Have an account? Login"}
                                </Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
                <Box mt={8}>
                    <Copyright />
                </Box>
            </Container>
        );
    }
}

export default withStyles(styles, { withTheme: true })(Register);
