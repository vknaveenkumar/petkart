import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import { withStyles } from "@material-ui/core/styles";
import axios from 'axios';
// import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Navbar from '../components/Navbar/Navbar'
import Layout from '../components/Layout/Layout'
import { BASE_URL } from '../../config/env'


/**
 * @param {*} theme 
 * This is Landing Dashboard Component
 */


const styles = theme => ({
    root: {
        backgroundColor: "red"
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    cardBody :{
        height: '100px',
        overflow: 'hidden'
    }

});

export class Login extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            products: [],
            cartItems: { id: 0, userId: 0, cart: [] },
            searchFilter: ''
        }
    }

    /**
     * Loads the products and cart items
     */
    componentDidMount() {
        axios.get(`${BASE_URL}/products`).then(res => {
            this.setState({
                products: res.data
            })

        })


        if (localStorage.getItem('petKartUserInfo') !== null && localStorage.getItem('petKartUserInfo') !== undefined) {
            const userId = JSON.parse(localStorage.getItem('petKartUserInfo')).id;
            axios.get(`${BASE_URL}/carts?userId=${userId}`).then(res => {
                // axios.get(`http://localhost:3001/carts/userId=${userId}`).then(res => {
                //console.log("ssssss", res.data[0])
                this.setState({
                    cartItems: res.data[0]
                })
            })
        }
    }

    /**
     * 
     * @param {*} item 
     * Add items to the cart
     * IF the user is not logged in ,then it navigate to login page
     */
    addToCart = (item) => {
        const { cartItems } = this.state

        if (localStorage.getItem('petKartUserInfo') === undefined || localStorage.getItem('petKartUserInfo') === null) {
            return this.props.history.push('/')
        }
        cartItems.cart.push(item)
        axios.put(`${BASE_URL}/carts/${this.state.cartItems.id}`, {
            id: cartItems.id,
            cart: cartItems.cart,
            userId: cartItems.userId
        }).then(() => {
            this.setState({
                cartItems
            })
        })
    }



    /**
     * 
     * @param {*} event 
     * on Change Filter
     */
    handleSearchFilter = (event) => {
        this.setState({
            searchFilter: event.target.value
        })
    }

    /**
     * 
     * @param {*} products 
     * This method filter the products based on search input
     */
    renderFilteredProduct = (products) => {
        const { searchFilter } = this.state;
        if (searchFilter === '' || searchFilter === null) {
            return products
        }
        let filteredText = products.filter(message => message.name.toUpperCase().indexOf(searchFilter.toUpperCase()) >= 0)
        return filteredText
    }

    renderCardFooter = (item) => {
        const { cartItems } = this.state
        return (
            <React.Fragment>
                {
                    <Button size="small" color="secondary" onClick={() => { this.addToCart(item) }}>
                        {cartItems.cart.some(data => data.id === item.id) ? "" : <React.Fragment>ADD TO CART<AddShoppingCartIcon /></React.Fragment>}
                    </Button>
                }
            </React.Fragment>
        )
    }



    render() {
        const { classes } = this.props;
        const { products, cartItems } = this.state
        let filteredProducts = this.renderFilteredProduct(products)

      
        return (
            <React.Fragment>
                <Navbar {...this.props} cartItems={cartItems.cart} noOfItemsInCart={cartItems.cart.length} searchFilter={this.handleSearchFilter} removeFromCart={this.removeFromCart} />
                <div style={{ marginTop: "25px" }}></div>
                {
                    cartItems.cart !== undefined && cartItems.cart !== null && <Container >
                        <Layout classes={classes} items={filteredProducts} direction={"row"} cartItems={cartItems.cart} renderCardFooter={this.renderCardFooter} dashboard={true} addToCart={this.addToCart} />
                    </Container >
                }
            </React.Fragment>
        );
    }
}

export default withStyles(styles, { withTheme: true })(Login);
