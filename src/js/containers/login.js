import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import axios from 'axios';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import { withStyles } from "@material-ui/core/styles";
// import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {BASE_URL} from '../../config/env'

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                Your Website
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}



const styles = theme => ({
    root: {
        backgroundColor: "red"
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },

});

export class Login extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            onSubmit: false,
            loginError: false
        }
    }

    _handleTextFieldChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    _handleLogin = () => {
        const { username, password } = this.state


        this.setState({
            onSubmit: true
        })

        axios.get(`${BASE_URL}/users?username=${username}`).then(res => {
            const { data } = res;
    
            if (data.length > 0 && password === data[0].password) {
                localStorage.setItem('petKartUserInfo', JSON.stringify(data[0]));
                this.props.history.push('/dashboard')
                return
            }
            this.setState({
                loginError: true
            })
        })
    }

    render() {
        const { classes } = this.props;
        const { onSubmit, username, password, loginError } = this.state
    
        return (
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>

                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign in
                    </Typography>
                    <form className={classes.form} noValidate>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="User Name"
                            name="username"
                            autoComplete="username"
                            value={username}
                            onChange={this._handleTextFieldChange}
                            autoFocus
                            error={onSubmit && username.length <= 0}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            value={this.state.password}
                            onChange={this._handleTextFieldChange}
                            autoComplete="current-password"
                            error={onSubmit && password.length <= 0}
                        />

                        {loginError &&
                            <Typography variant="body2" color="error" component="p">
                                *Invalid username or password
                        </Typography>}

                        <Button
                            fullWidth
                            variant="contained"
                            color="primary"
                            onClick={this._handleLogin}
                            className={classes.submit}
                        >
                            Sign In
                        </Button>
                        <Grid container>
                            <Grid item>
                                <Link href="/register" variant="body2">
                                    {"Don't have an account? Sign Up"}
                                </Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
                <Box mt={8}>
                    <Copyright />
                </Box>
            </Container>
        );
    }
}

export default withStyles(styles, { withTheme: true })(Login);
