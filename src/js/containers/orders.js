import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import { withStyles } from "@material-ui/core/styles";
import axios from 'axios';
// import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Icon from '@material-ui/core/Icon';
import SaveIcon from '@material-ui/icons/Save';
import Container from '@material-ui/core/Container';
import Navbar from '../components/Navbar/Navbar'
import PetTable from '../components/Table/Table'
import {BASE_URL} from '../../config/env'


/**
 * @param {*} theme 
 * This is Landing Dashboard Component
 */

const createData = (name, calories, fat, carbs, protein) => {
    return { name, calories, fat, carbs, protein };
}

const styles = theme => ({
    root: {
        flexGrow: 1,
        marginTop: '25px'
    },
    paper: {
        marginTop: theme.spacing(1),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },

    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    table: {
        minWidth: 300,
        //'& thead':{
        //  }
        '& thead':{
            backgroundColor: 'blue',
        },
        '& tbody >tr:nth-child(odd)':{
            backgroundColor: 'silver'
        }
    },
});

export class Login extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            products: [],
            cartItems: { id: 0, userId: 0, cart: [] },
            searchFilter: '',
            orders: []
        }
    }

    componentDidMount() {
        if (localStorage.getItem('petKartUserInfo') !== null && localStorage.getItem('petKartUserInfo') !== undefined) {
            const userId = JSON.parse(localStorage.getItem('petKartUserInfo')).id;
            axios.get(`${BASE_URL}/carts?userId=${userId}`).then(res => {
                this.setState({
                    cartItems: res.data[0]
                })
            })

            axios.get(`${BASE_URL}/orders?userId=${userId}`).then(res => {
                this.setState({
                    orders: res.data
                })
            })

        }
    }

    /**
     * 
     * @param {*} event 
     * on Change Filter
     */
    handleSearchFilter = (event) => {
        this.setState({
            searchFilter: event.target.value
        })
    }
    /**
     * 
     * @param {*} products 
     * This method filter the products based on search input
     */
    renderFilteredProduct = (products) => {
        const { searchFilter } = this.state;
        if (searchFilter === '' || searchFilter === null) {
            return products
        }
        let filteredText = products.filter(message => message.name.toUpperCase().indexOf(searchFilter.toUpperCase()) >= 0)
        return filteredText
    }


    renderTableHeader = () => {
        return (
            <TableRow>
                <TableCell>Name</TableCell>
                <TableCell align="right">Item Count</TableCell>
                <TableCell align="right">Date</TableCell>
            </TableRow>
        )
    }



    renderTableBody = () => {
        const { orders } = this.state
        return (
            <React.Fragment>
                {
                    orders.map((row) => (
                        <TableRow key={row.name}>
                            <TableCell component="th" scope="row">
                                {row.id}
                            </TableCell>
                            <TableCell align="right">{row.items.length}</TableCell>
                            <TableCell align="right">{new Date().toLocaleDateString()}</TableCell>
                        </TableRow>
                    ))

                }
            </React.Fragment>
        )
    }


    render() {
        const { classes } = this.props;
        const { cartItems, orders } = this.state


        let filteredProducts = this.renderFilteredProduct(cartItems.cart)
        return (
            <React.Fragment>
                <Navbar {...this.props} cartItems={cartItems.cart} noOfItemsInCart={cartItems.cart.length} searchFilter={this.handleSearchFilter} removeFromCart={this.removeFromCart} />
                <Grid container className={classes.root} spacing={2}>
                    <Grid item xs={12}>
                        <Grid container justify="flex-start" spacing={2}>
                            <PetTable classes={classes} tableHeader={this.renderTableHeader()} tableBody={this.renderTableBody()} />
                        </Grid>
                    </Grid>
                </Grid>
            </React.Fragment>
        );
    }
}

export default withStyles(styles, { withTheme: true })(Login);
