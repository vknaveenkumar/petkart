import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import { withStyles } from "@material-ui/core/styles";
import CardActions from '@material-ui/core/CardActions';
import RemoveShoppingCartIcon from '@material-ui/icons/RemoveShoppingCart';
import CardContent from '@material-ui/core/CardContent';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';



const styles = theme => ( {
  cardWidth: {
    maxWidth: 345,
  },
  media: {
    height: 140,
  },
} );

 function MediaCard(props) {


  const { item: { id, name, description }, dashboard, renderCardFooter,classes } = props

  console.log(classes)

  return (
    <Card className={classes.cardWidth}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image="https://images.dog.ceo/breeds/spaniel-brittany/n02101388_5977.jpg"
          title="Contemplative Reptile"
        />
        <CardContent >
          <Typography gutterBottom variant="h5" component="h2">
            {name}
          </Typography>
          {
            dashboard && <Typography variant="body2" className={classes.cardBody} color="textSecondary" component="p">
              {description}
            </Typography>
          }
        </CardContent>
      </CardActionArea>
      <CardActions>
        {renderCardFooter(props.item)}
      </CardActions>
    </Card>
  );
}


export default withStyles( styles )( MediaCard );
